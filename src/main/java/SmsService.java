import com.twilio.twiml.MessagingResponse;
import com.twilio.twiml.messaging.Body;
import com.twilio.twiml.messaging.Message;
import static spark.Spark.post;

public class SmsService {

    public static void main(String[] args) {
        //get("/", (req, res) -> "Hello Web");

        post("/receive-sms", (req, res) -> {
            res.type("application/xml");
            Body body = new Body
                    .Builder("Insert your response message here")
                    .build();

            Message sms = new Message
                    .Builder()
                    .body(body)
                    .build();

            System.out.println(req.body());

            MessagingResponse twiml = new MessagingResponse
                    .Builder()
                    .message(sms)
                    .build();
            return twiml.toXml();
        });
    }
}
